package com.example.activitytest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_item:
                Toast.makeText(this, "You clicked Add", Toast.LENGTH_SHORT).show();
                break;
            case R.id.remove_item:
                Toast.makeText(this, "You clicked Remove", Toast.LENGTH_SHORT).show();
                break;
            default:
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_layout);
        Button button1 = (Button) findViewById(R.id.button_1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Intent intent = new Intent(FirstActivity.this,FirstActivity.class);  //重新创建一个FirstActivity
                //Toast.makeText(FirstActivity.this,"You cliched Button 1",Toast.LENGTH_SHORT).show();
                  Intent intent = new Intent(FirstActivity.this,SecondActivity.class);
              //  Intent intent = new Intent("com.example.activitytest.ACTION_START");
              //  intent.addCategory("com.example.activitytest.MY_CATEGORY");
              //  startActivity(intent);
                  Intent intent = new Intent(Intent.ACTION_VIEW);
                  intent.setData(Uri.parse("http://www.baidu.com"));
                  startActivity(intent);
//                 String data = "Hello SecondActivity";
//                 Intent intent = new Intent("com.example.activitytest.ACTION_START");
//                 intent.putExtra("extra_data",data);
//                 startActivity(intent);

            }
        });
    }
}
